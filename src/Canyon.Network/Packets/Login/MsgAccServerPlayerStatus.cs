﻿namespace Canyon.Network.Packets.Login
{
    public class MsgAccServerPlayerStatus
    {
        public string RealmName { get; set; }
        public int Count => Status.Count;
        public List<PlayerStatus> Status { get; set; } = new();

        public struct PlayerStatus
        {
            public uint Identity;
            public uint AccountIdentity;
            public bool Online;
            public bool Deleted;
        }
    }
}