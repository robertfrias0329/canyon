﻿namespace Canyon.Network.Packets.Login
{
    public class MsgAccServerGameInformation
    {
        public int Status { get; set; }
        public int PlayerCount { get; set; }
        public int PlayerCountRecord { get; set; }
        public int PlayerLimit { get; set; }
    }
}