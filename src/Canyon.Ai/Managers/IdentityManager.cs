﻿using Canyon.Ai.States;

namespace Canyon.Ai.Managers
{
    public sealed class IdentityManager
    {
        public static IdentityManager Monster = new(Role.MONSTERID_FIRST, Role.MONSTERID_LAST);

        private readonly Queue<long> queue = new();
        private readonly long max = uint.MaxValue;
        private readonly long min;

        private object IdentityLock = new();

        public IdentityManager(long min, long max)
        {
            this.min = min;
            this.max = max;

            for (long i = this.min; i <= this.max; i++)
            {
                queue.Enqueue(i);
            }
        }

        public long GetNextIdentity
        {
            get
            {
                lock (IdentityLock)
                {
                    if (queue.TryDequeue(out long result))
                    {
                        return result;
                    }
                    return 0;
                }
            }
        }

        public void ReturnIdentity(long id)
        {
            if (id < min || id > max)
            {
                return;
            }

            lock (IdentityLock)
            {
                if (!queue.Contains(id))
                {
                    queue.Enqueue(id);
                }
            }
        }

        public int IdentitiesCount()
        {
            lock (IdentityLock)
            {
                return queue.Count;
            }
        }
    }
}