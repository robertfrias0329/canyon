﻿using Canyon.Database.Entities;

namespace Canyon.Ai.States
{
    public sealed class MonsterMagic
    {
        private readonly TimeOutMS timeOutMS = new();
        private readonly TimeOutMS warningTimeOutMS = new();

        public MonsterMagic(DbMonsterTypeMagic magic)
        {
            MonsterType = magic.MonsterType;
            MagicType = magic.MagicType;
            MagicLev = magic.MagicLev;
            ColdTime = magic.ColdTime;
            WarningTime = magic.WarningTime;
            StatusMagicLev = magic.StatusMagicLev;
            StatusMagicType = magic.StatusMagicType;
            LastTick = 0;

            timeOutMS.Startup((int)ColdTime);
        }

        public uint MonsterType { get; }
        public uint MagicType { get; }
        public uint MagicLev { get; }
        public uint ColdTime { get; }
        public ushort WarningTime { get; }
        public uint StatusMagicType { get; }
        public uint StatusMagicLev { get; }
        public long LastTick { get; set; }

        public bool IsReady()
        {
            return timeOutMS.IsTimeOut();
        }

        public void Use()
        {
            timeOutMS.Startup((int)ColdTime);
            LastTick = Environment.TickCount64;
        }

        public void StartWarningTimer()
        {
            warningTimeOutMS.Startup((int)WarningTime);
        }

        public bool IsWarningTimeOut()
        {
            return warningTimeOutMS.IsActive() && warningTimeOutMS.IsTimeOut();
        }

        public void ResetWarningTimeOut()
        {
            warningTimeOutMS.Clear();
        }
    }
}
