﻿using Canyon.Game.States;

namespace Canyon.Game.Services.Managers
{
    public sealed class IdentityManager
    {
        public static IdentityManager MapItem = new(Role.MAPITEM_FIRST, Role.MAPITEM_LAST);
        public static IdentityManager Pet = new(Role.CALLPETID_FIRST, Role.CALLPETID_LAST);
        public static IdentityManager Furniture = new(Role.SCENE_NPC_MIN, Role.SCENE_NPC_MAX);
        public static IdentityManager Traps = new(Role.MAGICTRAPID_FIRST, Role.MAGICTRAPID_LAST);

        public static IdentityManager Instances = new(100_000_000, 101_000_000);

        private readonly Queue<long> queue = new();
        private readonly long max = uint.MaxValue;
        private readonly long min;

        private object IdentityLock = new();

        public IdentityManager(long min, long max)
        {
            this.min = min;
            this.max = max;

            for (long i = this.min; i <= this.max; i++)
            {
                queue.Enqueue(i);
            }
        }

        public long GetNextIdentity
        {
            get
            {
                lock (IdentityLock)
                {
                    if (queue.TryDequeue(out long result))
                    {
                        return result;
                    }
                    return 0;
                }
            }
        }

        public void ReturnIdentity(long id)
        {
            if (id < min || id > max)
            {
                return;
            }

            lock (IdentityLock)
            {
                if (!queue.Contains(id))
                {
                    queue.Enqueue(id);
                }
            }
        }

        public int IdentitiesCount() 
        {
            lock (IdentityLock)
            {
                return queue.Count;
            }
        }
    }
}
