﻿using Canyon.Database.Entities;
using Canyon.Game.Database;
using Canyon.Game.Database.Repositories;
using Canyon.Game.Sockets.Game.Packets;
using Canyon.Game.States;
using Canyon.Game.States.NPCs;
using Canyon.Game.States.User;
using Canyon.Game.States.World;
using Canyon.Network.Packets.Login;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Drawing;

namespace Canyon.Game.Services.Managers
{
    public class RoleManager
    {
        private static readonly ILogger logger = LogFactory.CreateLogger<RoleManager>();
        private static readonly ConcurrentDictionary<uint, Character> userSet = new();
        private static readonly ConcurrentDictionary<uint, Role> roleSet = new();
        private static readonly ConcurrentDictionary<uint, DbSuperman> superman = new();
        private static readonly ConcurrentDictionary<uint, DbMonstertype> monsterTypes = new();
        private static readonly ConcurrentDictionary<uint, DbMonsterTypeMagic> monsterMagics = new();

        private static bool isShutdown;
        private static bool isMaintenanceEntrance;
        private static bool isCooperatorMode;

        private RoleManager() { }

        public static int OnlineUniquePlayers => userSet.Values.Select(x => x.Client.MacAddress).Distinct().Count();
        public static int OnlinePlayers => userSet.Count;
        public static int RolesCount => roleSet.Count;

        public static int MaxOnlinePlayers { get; private set; }

        public static async Task InitializeAsync()
        {
            logger.LogInformation("Starting Role Manager");

            var supermen = await SupermanRepository.GetAsync();
            foreach (var superman in supermen)
            {
                RoleManager.superman.TryAdd(superman.UserIdentity, superman);
            }

            foreach (DbMonstertype mob in await MonsterypeRepository.GetAsync())
            {
                monsterTypes.TryAdd(mob.Id, mob);
            }

            foreach (DbMonsterTypeMagic magic in await MonsterTypeMagicRepository.GetAsync())
            {
                monsterMagics.TryAdd(magic.Id, magic);
            }
        }

        public static async Task<bool> LoginUserAsync(Client user)
        {
            if (isShutdown)
            {
                await user.DisconnectWithMessageAsync(MsgConnectEx<Client>.RejectionCode.ServerDown);
                return false;
            }

            if (isMaintenanceEntrance)
            {
                await user.DisconnectWithMessageAsync(MsgConnectEx<Client>.RejectionCode.ServerLocked);
                return false;
            }

            if (isCooperatorMode)
            {
                await user.DisconnectWithMessageAsync(MsgConnectEx<Client>.RejectionCode.NonCooperatorAccount);
                return false;
            }

            if (userSet.TryGetValue(user.Character.Identity, out Character concurrent))
            {
                logger.LogInformation($"User {user.Character.Identity} {user.Character.Name} tried to login an already connected client.");

                string message;
                if (user.IpAddress != concurrent.Client.IpAddress)
                {
                    message = StrAnotherLoginSameIp;
                }
                else
                {
                    message = StrAnotherLoginOtherIp;
                }

                await concurrent.Client.DisconnectWithMessageAsync(message);
                user.Disconnect();
                return false;
            }

            if (userSet.Count > ServerConfiguration.Configuration.Realm.MaxOnlinePlayers && user.AuthorityLevel <= 1 && !user.Character.IsGm())
            {
                await user.DisconnectWithMessageAsync(MsgConnectEx<Client>.RejectionCode.ServerFull);
                logger.LogInformation($"{user.Character.Name} tried to login and server is full.");
                return false;
            }

            userSet.TryAdd(user.Character.Identity, user.Character);
            roleSet.TryAdd(user.Character.Identity, user.Character);

            await user.Character.SetLoginAsync();

            logger.LogInformation($"{user.Character.Name} has logged in.");

            await Kernel.Sockets.LoginClient.CallAsync<object>("ChangePlayerStatusAsync", new MsgAccServerPlayerStatus
            {
                RealmName = ServerConfiguration.Configuration.Realm.Name,
                Status = new List<MsgAccServerPlayerStatus.PlayerStatus>
                {
                    new() {Identity = user.AccountIdentity, Online = true}
                }
            });

            await EventManager.OnLoginAsync(user.Character);

            if (OnlinePlayers > MaxOnlinePlayers)
            {
                MaxOnlinePlayers = OnlinePlayers;
            }

            return true;
        }

        public static void ForceLogoutUser(uint idUser)
        {
            userSet.TryRemove(idUser, out _);
            roleSet.TryRemove(idUser, out _);
        }

        public static void SetMaintenanceStart()
        {
            isMaintenanceEntrance = true;
        }

        public static void ToggleCooperatorMode()
        {
            isCooperatorMode = !isCooperatorMode;
            if (isCooperatorMode)
            {
                logger.LogInformation("Cooperator mode has been enabled! Only cooperators accounts will be able to login.");
            }
            else
            {
                logger.LogInformation("Cooperator mode has been disabled! All accounts are enabled to login.");
            }
        }

        public static async Task KickOutAsync(uint idUser, string reason = "")
        {
            if (userSet.TryGetValue(idUser, out Character user))
            {
                if (!user.IsDeleted)
                {
                    await user.SaveAsync();
                }
                await user.Client.DisconnectWithMessageAsync(string.Format(StrKickout, reason));
                logger.LogInformation($"User {user.Name} has been kicked: {reason}");
            }
        }

        public static async Task KickOutAllAsync(string reason = "", bool isShutdown = false)
        {
            if (isShutdown)
            {
                RoleManager.isShutdown = true;
            }

            foreach (Character user in userSet.Values)
            {
                await user.Client.DisconnectWithMessageAsync(string.Format(StrKickout, reason));
                logger.LogInformation($"User {user.Name} has been kicked (kickoutall): {reason}");
            }
        }

        public static Character GetUserByAccount(uint idAccount)
        {
            return userSet.Values.FirstOrDefault(x => x.Client.AccountIdentity == idAccount);
        }

        public static Character GetUser(uint idUser)
        {
            return userSet.TryGetValue(idUser, out Character client) ? client : null;
        }

        public static Character GetUser(string name)
        {
            return userSet.Values.FirstOrDefault(x => x.Name == name);
        }

        public static int CountUserByMacAddress(string macAddress)
        {
            return userSet.Values.Count(x => macAddress.Equals(x.Client?.MacAddress, StringComparison.InvariantCultureIgnoreCase));
        }

        public static List<T> QueryRoleByMap<T>(uint idMap) where T : Role
        {
            return roleSet.Values.Where(x => x.MapIdentity == idMap && x is T).Cast<T>().ToList();
        }

        public static List<T> QueryRoleByType<T>() where T : Role
        {
            return roleSet.Values.Where(x => x is T).Cast<T>().ToList();
        }

        public static List<Character> QueryUserSetByMap(uint idMap)
        {
            return userSet.Values.Where(x => x.MapIdentity == idMap).ToList();
        }

        public static List<Character> QueryUserSet()
        {
            return userSet.Values.ToList();
        }

        /// <summary>
        ///     Attention, DO NOT USE to add <see cref="Character" />.
        /// </summary>
        public static bool AddRole(Role role)
        {
            return roleSet.TryAdd(role.Identity, role);
        }

        public static Role GetRole(uint idRole)
        {
            return roleSet.TryGetValue(idRole, out Role role) ? role : null;
        }

        public static List<Role> QueryRoles(Func<Role, bool> predicate)
        {
            return roleSet.Values.Where(predicate).ToList();
        }

        public static T GetRole<T>(uint idRole) where T : Role
        {
            return roleSet.TryGetValue(idRole, out Role role) ? role as T : null;
        }

        public static T GetRole<T>(Func<T, bool> predicate) where T : Role
        {
            return roleSet.Values
                            .Where(x => x is T)
                            .Cast<T>()
                            .FirstOrDefault(x => predicate != null && predicate(x));
        }

        public static T FindRole<T>(uint idRole) where T : Role
        {
            foreach (GameMap map in MapManager.GameMaps.Values)
            {
                var result = map.QueryRole<T>(idRole);
                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }

        public static T FindRole<T>(Func<T, bool> predicate) where T : Role
        {
            foreach (GameMap map in MapManager.GameMaps.Values)
            {
                T result = map.QueryRole(predicate);
                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }

        /// <summary>
        ///     Attention, DO NOT USE to remove <see cref="Character" />.
        /// </summary>
        public static bool RemoveRole(uint idRole)
        {
            return roleSet.TryRemove(idRole, out _);
        }

        public static async Task AddOrUpdateSupermanAsync(uint idUser, int amount)
        {
            if (!superman.TryGetValue(idUser, out var sm))
            {
                superman.TryAdd(idUser, sm = new DbSuperman
                {
                    UserIdentity = idUser
                });
            }

            sm.Amount = (uint)amount;
            await ServerDbContext.SaveAsync(sm);
        }

        public static int GetSupermanPoints(uint idUser)
        {
            return (int)(superman.TryGetValue(idUser, out var value) ? value.Amount : 0);
        }

        public static int GetSupermanRank(uint idUser)
        {
            int result = 1;
            foreach (var super in superman.Values.OrderByDescending(x => x.Amount))
            {
                if (super.UserIdentity == idUser)
                {
                    return result;
                }

                result++;
            }
            return result;
        }

        public static DbMonstertype GetMonstertype(uint type)
        {
            return monsterTypes.TryGetValue(type, out DbMonstertype mob) ? mob : null;
        }

        public static List<DbMonsterTypeMagic> GetMonsterMagics(uint type)
        {
            return monsterMagics.Values.Where(x => x.MonsterType == type).ToList();
        }

        public static bool IsValidName(string szName)
        {
            if (long.TryParse(szName, out _))
            {
                return false;
            }

            foreach (var c in szName)
            {
                if (c < ' ')
                {
                    return false;
                }

                switch (c)
                {
                    case ' ':
                    case ';':
                    case ',':
                    case '/':
                    case '\\':
                    case '=':
                    case '%':
                    case '@':
                    case '\'':
                    case '"':
                    case '[':
                    case ']':
                    case '?':
                    case '{':
                    case '}':
                        return false;
                }
            }

            string lower = szName.ToLower();
            return InvalidNames.All(part => !lower.Contains(part));
        }

        private static readonly string[] InvalidNames =
        {
            "{", "}", "[", "]", "(", ")", "\"", "[gm]", "[pm]", "'", "´", "`", "admin", "helpdesk", " ",
            "bitch", "puta", "whore", "ass", "fuck", "cunt", "fdp", "porra", "poha", "caralho", "caraio"
        };

        public static long RoleTimerTicks { get; private set; }
        public static int ProcessedRoles { get; private set; }

        public static async Task OnRoleTimerAsync()
        {
            int processedRoles = 0;
            Stopwatch sw = Stopwatch.StartNew();
            foreach (var role in roleSet.Values.Where(x => x is not Character && x is not BaseNpc))
            {
                await role.OnTimerAsync();
                processedRoles++;
            }
            sw.Stop();
            ProcessedRoles = processedRoles;
            RoleTimerTicks = sw.ElapsedTicks;
        }
    }
}
