﻿using System.Collections.Concurrent;
using System.Diagnostics;
using Canyon.Database.Entities;
using Canyon.Game.Database;
using Canyon.Game.Database.Repositories;
using Canyon.Game.States;
using Canyon.Game.States.Events;
using Canyon.Game.States.Events.Elite;
using Canyon.Game.States.Events.Interfaces;
using Canyon.Game.States.Events.Mount;
using Canyon.Game.States.Events.Qualifier.TeamQualifier;
using Canyon.Game.States.Events.Qualifier.UserQualifier;
using Canyon.Game.States.Items;
using Canyon.Game.States.NPCs;
using Canyon.Game.States.User;

namespace Canyon.Game.Services.Managers;

public class EventManager
{
    private static readonly ILogger logger = LogFactory.CreateLogger<EventManager>();

    private static readonly ConcurrentDictionary<uint, DbAction> actions = new();
    private static readonly ConcurrentDictionary<uint, DbTask> tasks = new();
    private static readonly ConcurrentDictionary<GameEvent.EventType, GameEvent> events = new();
    private static readonly List<QueuedAction> queuedActions = new();

    private static ConcurrentDictionary<int, List<uint>> inviteTransInvitations = new();

    private static readonly TimeOut rankingBroadcast = new(10);

    private static DbDailyReset dailyReset;

    private EventManager()
    {
    }

    public static async Task<bool> InitializeAsync()
    {
        logger.LogInformation("Loading actions and tasks");

        await LoadActionsAsync();

        logger.LogInformation("Loading NPCs");

        foreach (DbNpc dbNpc in await NpcRepository.GetAsync())
        {
            Npc npc = new Npc(dbNpc);

            if (!await npc.InitializeAsync())
            {
                logger.LogWarning($"Could not load NPC {dbNpc.Id} {dbNpc.Name}");
                continue;
            }

            await npc.EnterMapAsync();
            if (npc.Task0 != 0 && !tasks.ContainsKey(npc.Task0))
                logger.LogWarning($"Npc {npc.Identity} {npc.Name} no task found [taskid: {npc.Task0}]");
        }

        foreach (DbDynanpc dbDynaNpc in await DynamicNpcRespository.GetAsync())
        {
            try
            {
                DynamicNpc npc = new DynamicNpc(dbDynaNpc);
                if (!await npc.InitializeAsync())
                {
                    logger.LogWarning($"Could not load NPC {dbDynaNpc.Id} {dbDynaNpc.Name}");
                    continue;
                }

                await npc.EnterMapAsync();
                if (npc.Task0 != 0 && !tasks.ContainsKey(npc.Task0))
                    logger.LogWarning($"Npc {npc.Identity} {npc.Name} no task found [taskid: {npc.Task0}]");
            }
            catch (Exception ex)
            {
                logger.LogCritical(ex, "Error initializing NPC({},{})! Message: {}", dbDynaNpc.Id, dbDynaNpc.Name, ex.Message);
            }
        }

        await RegisterEventAsync(new ArenaQualifier());
        await RegisterEventAsync(new QuizShow());
        await RegisterEventAsync(new FamilyWar());
        await RegisterEventAsync(new CaptureTheFlag());
        await RegisterEventAsync(new HorseRacing());
        await RegisterEventAsync(new LineSkillPK());

#if DEBUG
        await RegisterEventAsync(new ElitePkTournament());
        await RegisterEventAsync(new TeamArenaQualifier());
#endif

        dailyReset = await DailyResetRepository.GetLatestAsync();
        if (dailyReset != null)
        {
            logger.LogInformation($"Latest daily reset: {dailyReset.RunTime}");
        }
        else
        {
            logger.LogInformation("No daily resets yet.");
        }
        return true;
    }

    public static async Task LoadActionsAsync()
    {
        tasks.Clear();
        foreach (DbTask task in await TaskRepository.GetAsync()) tasks.TryAdd(task.Id, task);

        logger.LogInformation("Loaded {Tasks} tasks from database", tasks.Count);

        actions.Clear();
        foreach (DbAction action in await ActionRepository.GetAsync())
        {
            if (action.Type == 102)
            {
                var response = action.Param.Split(' ');
                if (response.Length < 2)
                {
                    logger.LogWarning($"Action [{action.Id}] Type 102 doesn't set a task [param: {action.Param}]");
                    action.Param = action.Param.Trim() + " 0";
                    await ServerDbContext.SaveAsync(action);
                }
                else if (response[1] != "0")
                    if (!uint.TryParse(response[1], out var taskId) || !tasks.ContainsKey(taskId))
                        logger.LogWarning($"Task not found for action {action.Id}");
            }

            if (action.Type == 1001 && action.Param.StartsWith("profession"))
            {
                string[] strings = action.Param.Split(" ");
                string p = strings[0];
                string opt = strings[1];
                int value = int.Parse(strings[2]);
                if (value >= 1000)
                {
                    int profType = value / 1000;
                    int profLevel = value % 10;
                    value = profType * 10 + profLevel;

                    logger.LogDebug($"\"{action.Param}\" changed to \"{p} {opt} {value}\"");
                    action.Param = $"{p} {opt} {value}";
                    await ServerDbContext.SaveAsync(action);
                }
            }

            actions.TryAdd(action.Id, action);
        }

        logger.LogInformation("Loaded {Actions} actions from database", actions.Count);
    }

    public static async Task OnLoginAsync(Character user)
    {
        foreach (GameEvent e in events.Values) await e.OnLoginAsync(user);
    }

    public static async Task OnLogoutAsync(Character user)
    {
        foreach (GameEvent e in events.Values) await e.OnLogoutAsync(user);
    }

    public static DbAction GetAction(uint idAction)
    {
        return actions.TryGetValue(idAction, out DbAction result) ? result : null;
    }

    public static DbTask GetTask(uint idTask)
    {
        return tasks.TryGetValue(idTask, out DbTask result) ? result : null;
    }

    public static bool QueueAction(QueuedAction action)
    {
        queuedActions.Add(action);
        return true;
    }

    public static async Task<bool> RegisterEventAsync(GameEvent @event)
    {
        if (events.ContainsKey(@event.Identity)) return false;

        if (await @event.CreateAsync() && events.TryAdd(@event.Identity, @event))
        {
            logger.LogInformation($"Event '{@event.Name}' has been registered");
            return true;
        }

        logger.LogError($"Event '{@event.Name}' has not been registered");
        return false;
    }

    public static void RemoveEvent(GameEvent.EventType type)
    {
        events.TryRemove(type, out _);
    }

    public static List<IWitnessEvent> QueryWitnessEvents()
    {
        return events.Values.Where(x => x is IWitnessEvent).Cast<IWitnessEvent>().ToList();
    }

    public static List<T> QueryEvents<T>()
    {
        return events.Values.Where(x => x is T).Cast<T>().ToList();
    }

    public static T GetEvent<T>() where T : GameEvent
    {
        return events.Values.FirstOrDefault(x => x.GetType() == typeof(T)) as T;
    }

    public static GameEvent GetEvent(GameEvent.EventType type)
    {
        return events.TryGetValue(type, out GameEvent ev) ? ev : null;
    }

    public static GameEvent GetEvent(uint idMap)
    {
        return events.Values.FirstOrDefault(x => x.Map?.Identity == idMap);
    }

    public static void ClearInvitationList(int eventId)
    {
        if (inviteTransInvitations.TryGetValue(eventId, out var invitations))
        {
            inviteTransInvitations.Clear();
        }
    }

    public static void AddToInvitationList(int eventId, uint userId)
    {
        var invitations = inviteTransInvitations.GetOrAdd(eventId, new List<uint>());
        if (!invitations.Contains(userId))
        {
            invitations.Add(userId);
        }
    }

    public static List<uint> QueryInvitationList(int eventId)
    {
        return inviteTransInvitations.TryGetValue(eventId, out var result) ? result : new List<uint>();
    }

    public static async Task OnTimerAsync()
    {
        await PigeonManager.OnTimerAsync();
        await AuctionManager.OnTimerAsync();

        var ranking = rankingBroadcast.ToNextTime();
        foreach (DynamicNpc dynaNpc in RoleManager.QueryRoleByType<DynamicNpc>())
        {
            if (dynaNpc.IsGoal() || !dynaNpc.IsSynFlag()) continue;

            await dynaNpc.CheckFightTimeAsync();

            if (ranking && events.Values.All(x => x.Map?.Identity != dynaNpc.MapIdentity))
                await dynaNpc.BroadcastRankingAsync();
        }

        if (ranking)
        {
            _ = ServerStatisticManager.SaveAsync();
        }

        foreach (GameEvent @event in events.Values)
        {
            if (@event.ToNextTime())
            {
                await @event.OnTimerAsync();
            }
        }

        for (var i = queuedActions.Count - 1; i >= 0; i--)
        {
            QueuedAction action = queuedActions[i];
            if (action.CanBeExecuted)
            {
                Character user = RoleManager.GetUser(action.UserIdentity);
                Item item = null;
                Role role = null;
                if (user != null)
                {
                    if (user.InteractingItem != 0) item = user.UserPackage.FindByIdentity(user.InteractingItem);

                    if (user.InteractingNpc != 0) role = RoleManager.GetRole(user.InteractingNpc);

                    Task queuedActionTask()
                    {
                        return GameAction.ExecuteActionAsync(action.Action, user, role, item, "");
                    }

                    user.QueueAction(queuedActionTask);
                }

                queuedActions.RemoveAt(i);
            }
        }

        await SyndicateManager.OnTimerAsync();

        await DailyResetAsync();
    }

    private static bool IsDailyResetEnabled()
    {
        if (dailyReset == null)
        {
            // server has started previously but no daily reset has been made
            if (ServerConfiguration.Configuration.Realm.ReleaseDate < DateTime.Now)
            {
                return true;
            }
            return false;
        }

        // server was shutdown or midnight has passed (or daily reset has not been completed, duration = 0)
        if (dailyReset.RunTime.Date < DateTime.Now.Date || dailyReset.Duration == 0)
        {
            return true;
        }

        return false;
    }

    private static async Task DailyResetAsync()
    {
        if (!IsDailyResetEnabled())
            return;

        logger.LogInformation($"Running daily reset tasks");

        dailyReset = new DbDailyReset
        {
            RunTime = DateTime.Now
        };
        await ServerDbContext.SaveAsync(dailyReset);

        Stopwatch sw = Stopwatch.StartNew();

        try
        {
            foreach (var player in RoleManager.QueryUserSet()) // do online players
            {
                try
                {
                    await player.DoDailyResetAsync();
                }
                catch (Exception ex)
                {
                    logger.LogCritical(ex, "Error when reseting online user! Message: {}", ex.Message);
                }
            }

            await ServerDbContext.ScalarAsync($"UPDATE cq_jianghu_caltivate_times SET free_times=0, paid_times=0;");
            await ServerDbContext.ScalarAsync($"UPDATE task_detail SET " +
                $"data1=0, data2=0, data3=0, data4=0, " +
                $"data5=0, data6=0, data7=0, " +
                $"complete_flag=0 " +
                $"WHERE task_id IN ({string.Join(",", TaskDetail.DailyTasks.Select(x => x.ToString()).ToArray())});");
            await ServerDbContext.ScalarAsync("UPDATE cq_activity_user_task SET complete_flag=0, schedule=0;");

            foreach (var e in events.Values)
            {
                await e.OnDailyResetAsync();
            }

            await FlowerManager.DailyResetAsync();
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Error during daily reset! Message: {message}", ex.Message);
        }
        finally
        {
            sw.Stop();
            dailyReset.Duration = (ulong)sw.ElapsedTicks;
            await ServerDbContext.SaveAsync(dailyReset);

            logger.LogInformation($"Daily reset tasks finished in {sw.Elapsed.TotalMilliseconds:N3} ms");
        }
    }
}