﻿using Canyon.Database;
using Microsoft.Extensions.Configuration;

namespace Canyon.Game
{
    public class ServerConfiguration
    {
        public static ServerConfiguration Configuration { get; set; }

        public ServerConfiguration(params string[] args)
        {
            new ConfigurationBuilder()
                .AddJsonFile("Canyon.Game.Config.json")
                .AddCommandLine(args)
                .AddEnvironmentVariables()
                .Build()
                .Bind(this);
        }

        public DatabaseConfiguration Database { get; set; }
        public RealmConfiguration Realm { get; set; }
        public LoginConfiguration Login { get; set; }
        public AiConfiguration Ai { get; set; }
        public List<MaintenanceScheduleConfiguration> MaintenanceSchedule { get; set; }
        public bool CooperatorMode { get; set; } = false;

        public class RealmConfiguration
        {
            public Guid ServerId { get; set; }
            public string Name { get; set; }

            public string IPAddress { get; set; }
            public int Port { get; set; }

            public int MaxOnlinePlayers { get; set; }

            public string Username { get; set; }
            public string Password { get; set; }

            public DateTime ReleaseDate { get; set; }
        }

        public class LoginConfiguration
        {
            public string IPAddress { get; set; }
            public int ListenPort { get; set; }
            public int ConnectPort { get; set; }
        }

        public class AiConfiguration
        {
            public string IPAddress { get; set; }
            public int Port { get; set; }

            public string Username { get; set; }
            public string Password { get; set; }
        }

        public class MaintenanceScheduleConfiguration
        {
            public DayOfWeek DayOfWeek { get; set; }
            public string Time { get; set; }
            public int WarningMinutes { get; set; }
        }
    }
}
