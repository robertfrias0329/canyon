﻿using Canyon.Database.Entities;

namespace Canyon.Game.Database.Repositories
{
    public static class PeerageRepository
    {
        public static async Task<List<DbPeerage>> GetAsync()
        {
            await using var context = new ServerDbContext();
            return context.Peerage.ToList();
        }
    }
}
