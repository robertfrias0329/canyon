﻿using Canyon.Database.Entities;

namespace Canyon.Game.Database.Repositories
{
    public static class ActionRepository
    {
        public static async Task<List<DbAction>> GetAsync()
        {
            await using var db = new ServerDbContext();
            return db.Actions.ToList();
        }
    }
}
