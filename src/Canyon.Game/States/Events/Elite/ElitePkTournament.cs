﻿using Canyon.Game.Services.Managers;
using Canyon.Game.Sockets.Game.Packets;
using Canyon.Game.States.Events.Tournament;
using Canyon.Game.States.User;
using Canyon.Game.States.World;

namespace Canyon.Game.States.Events.Elite
{
    public sealed class ElitePkTournament : GameEvent
    {
        private static readonly ILogger logger = LogFactory.CreateLogger<ElitePkTournament>();

        public static readonly uint[] WaitingMaps =
        {
            2075,
            2076,
            2077,
            2078
        };

        private const uint BASE_PK_MAP_ID = 910000;

        private ElitePkGroupTournament[] groups = new ElitePkGroupTournament[4];

        public static readonly IdentityManager ElitePkMap = new(BASE_PK_MAP_ID, BASE_PK_MAP_ID + 9999);

        public ElitePkTournament() 
            : base("Elite PK Tournament", 1000)
        {
        }

        public override EventType Identity => EventType.ElitePkTournament;

        public static GameMap BasePkMap { get; private set; }

        public override async Task<bool> CreateAsync()
        {
            groups[0] = new ElitePkGroupTournament(new TournamentRules
            {
                MinLevel = 70,
                MaxLevel = 100,
                MinNobility = MsgPeerage.NobilityRank.Serf,
                MaxNobility = MsgPeerage.NobilityRank.King
            }, WaitingMaps[0], 0);
            await groups[0].InitializeAsync();

            groups[1] = new ElitePkGroupTournament(new TournamentRules
            {
                MinLevel = 100,
                MaxLevel = 119,
                MinNobility = MsgPeerage.NobilityRank.Serf,
                MaxNobility = MsgPeerage.NobilityRank.King
            }, WaitingMaps[1], 1);
            await groups[1].InitializeAsync();

            groups[2] = new ElitePkGroupTournament(new TournamentRules
            {
                MinLevel = 120,
                MaxLevel = 129,
                MinNobility = MsgPeerage.NobilityRank.Serf,
                MaxNobility = MsgPeerage.NobilityRank.King
            }, WaitingMaps[2], 2);
            await groups[2].InitializeAsync();

            groups[3] = new ElitePkGroupTournament(new TournamentRules
            {
                MinLevel = 130,
                MaxLevel = ExperienceManager.GetLevelLimit(),
                MinNobility = MsgPeerage.NobilityRank.Serf,
                MaxNobility = MsgPeerage.NobilityRank.King
            }, WaitingMaps[3], 3);
            await groups[3].InitializeAsync();

            BasePkMap = MapManager.GetMap(BASE_PK_MAP_ID);
            if (BasePkMap == null)
            {
                logger.LogWarning($"Could not start Elite PK Tournament! Base PK map not found.");
                return false;
            }

            return true;
        }

        public override bool IsAllowedToJoin(Role sender)
        {

            return base.IsAllowedToJoin(sender);
        }

        public override async Task OnEnterAsync(Character sender)
        {
            for (int i = 0; i < groups.Length; i++)
            {
                if (groups[i].IsParticipantAllowedToJoin(sender))
                {
                    await groups[i].InscribeAsync(new ElitePkParticipant(sender));
                    break;
                }
            }
        }

        public override Task OnExitAsync(Character sender)
        {
            return base.OnExitAsync(sender);
        }

        public bool IsAllowedToJoin(Character user, int group)
        {
            if (group < 0 || group > 3)
            {
                return false;
            }

            ElitePkGroupTournament elitePk = groups[group];
            return elitePk.IsParticipantAllowedToJoin(user);
        }

        public async Task SubmitEventWindowAsync(Character target, int group)
        {
            if (group < 0 || group > 3)
            {
                return;
            }

            await groups[group].SubmitEventWindowAsync(target);
        }
    }
}