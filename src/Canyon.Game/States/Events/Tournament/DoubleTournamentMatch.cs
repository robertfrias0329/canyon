﻿using Canyon.Game.States.Events.Interfaces;

namespace Canyon.Game.States.Events.Tournament
{
    public class DoubleTournamentMatch<TParticipant, TEntity>
        : BaseTournamentMatch<TParticipant, TEntity>
        where TParticipant : ITournamentEventParticipant<TEntity>
    {
        private readonly TParticipant participant1;
        private readonly TParticipant participant2;
        private TParticipant participant3;

        private SingleTournamentMatch<TParticipant, TEntity> match1;
        private SingleTournamentMatch<TParticipant, TEntity> match2;

        public DoubleTournamentMatch(int identity, int index)
            : base(identity, index)
        {
        }

        public TParticipant Participant3 { get; private set; }

        public SingleTournamentMatch<TParticipant, TEntity> Match1 => match1;
        public SingleTournamentMatch<TParticipant, TEntity> Match2 => match2;

        public void SetFirstMatch()
        {
            match1 = new SingleTournamentMatch<TParticipant, TEntity>(Identity, Index)
            {
                Participant1 = participant1,
                Participant2 = participant2
            };
        }

        public void SetSecondMatch(TParticipant participant3)
        {
            this.participant3 = participant3;
            match2 = new SingleTournamentMatch<TParticipant, TEntity>(Identity, Index)
            {
                Participant2 = this.participant3
            };
        }
    }
}
