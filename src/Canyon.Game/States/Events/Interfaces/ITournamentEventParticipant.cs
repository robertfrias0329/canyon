﻿namespace Canyon.Game.States.Events.Interfaces
{
    public interface ITournamentEventParticipant<TType>
    {
        uint Identity { get; }
        string Name { get; }

        TType Participant { get; }
        bool Bye { get; set; }

        ContestantFlag Flag { get; set; }

        public enum ContestantFlag : uint
        {
            None = 0,
            Fighting = 1,
            Lost = 2,
            Qualified = 3,
            Waiting = 4,
            Bye = 5,
            Inactive = 7,
            WonMatch = 8
        }
    }
}