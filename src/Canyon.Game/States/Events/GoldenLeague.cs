﻿namespace Canyon.Game.States.Events
{
    public sealed class GoldenLeague : GameEvent
    {
        private const uint BASE_MAP_ID = 960000u;

        public GoldenLeague() 
            : base("Champion Arena", 1000)
        {
        }
    }
}
