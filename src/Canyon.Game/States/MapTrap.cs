﻿using Canyon.Database.Entities;
using Canyon.Game.Database.Repositories;
using Canyon.Game.Services.Managers;
using Canyon.Game.Sockets.Game.Packets;
using Canyon.Game.States.Magics;
using Canyon.Game.States.NPCs;
using Canyon.Game.States.User;
using Canyon.Shared.Mathematics;
using System.Drawing;
using static Canyon.Game.Sockets.Game.Packets.MsgInteract;
using static Canyon.Game.Sockets.Game.Packets.MsgMapItem;

namespace Canyon.Game.States
{
    public sealed class MapTrap : Role
    {
        private static readonly ILogger logger = LogFactory.CreateLogger<MapTrap>();

        private readonly TimeOutMS fightTimer;
        private readonly TimeOut lifePeriodTimer;

        private DbTrap trap;
        private Role owner;
        private Magic magic;
        private int activeTimes;
        private Point castPosition;
        private Point destinationPoint;

        public MapTrap(DbTrap trap)
        {
            this.trap = trap;

            Identity = trap.Id;
            MapIdentity = trap.MapId;
            X = trap.PosX;
            Y = trap.PosY;

            lifePeriodTimer = new TimeOut();
            lifePeriodTimer.Clear();
            fightTimer = new TimeOutMS();
            fightTimer.Clear();
        }

        public uint Type => trap.TypeId;
        public uint IdAction => trap.Type.ActionId;
        public int ActiveTimes => activeTimes;
        public int RemainingActiveTimes { get; set; }
        public override byte Level
        {
            get => trap.Type.Level;
            set => trap.Type.Level = value;
        }
        public override int MinAttack => trap.Type.AttackMin;
        public override int MaxAttack => trap.Type.AttackMax;
        public override int MagicAttack => trap.Type.AttackMax;
        public TargetType AttackMode => (TargetType)trap.Type.AtkMode;
        public override int AttackSpeed => trap.Type.AttackSpeed;
        public ushort MagicType => trap.Type.MagicType;
        public int MagicHitRate => trap.Type.MagicHitrate;
        public int Size => trap.Type.Size;

        public TrapTypeSort Sort => (TrapTypeSort)trap.Type.Sort;

        public bool IsTrapSort => trap.Type.Sort <= 10;

        public bool IsAutoSort => !IsTrapSort;

        public override bool IsAlive => RemainingActiveTimes > 0;

        public bool IsInRange(Role target)
        {
            return GetDistance(target) <= Size;
        }

        #region Initialization

        public async Task<bool> InitializeActionAsync(Role owner)
        {
            trap.Type ??= await TrapTypeRepository.GetAsync(trap.TypeId);

            if (trap.Type == null)
            {
                logger.LogInformation($"Trap has no type {trap.Type} [TrapId: {trap.Id}]");
                return false;
            }

            this.owner = owner;

            RemainingActiveTimes = activeTimes = trap.Type.ActiveTimes;

            fightTimer.SetInterval(trap.Type.AttackSpeed);

            Mesh = trap.Look;

            idMap = trap.MapId;
            currentX = trap.PosX;
            currentY = trap.PosY;
            return true;
        }

        public async Task<bool> InitializeAsync(Role owner = null, Magic magic = null)
        {
            trap.Type ??= await TrapTypeRepository.GetAsync(trap.TypeId);

            if (trap.Type == null)
            {
                logger.LogInformation($"Trap has no type {trap.Type} [TrapId: {trap.Id}]");
                return false;
            }

            this.owner = owner;
            this.magic = magic;

            if (magic != null && trap.Type.ActiveTimes == 0)
            {
                RemainingActiveTimes = activeTimes = (int)magic.ActiveTimes;
            }
            else
            {
                RemainingActiveTimes = activeTimes = trap.Type.ActiveTimes;
            }

            if (owner != null)
            {
                castPosition = new Point(owner.X, owner.Y);
            }
            else
            {
                castPosition = new Point(trap.PosX, trap.PosY);
            }

            fightTimer.SetInterval(trap.Type.AttackSpeed);

            Mesh = trap.Look;

            idMap = trap.MapId;
            currentX = trap.PosX;
            currentY = trap.PosY;

            await EnterMapAsync();
            return true;
        }

        #endregion

        #region Map

        public override Task EnterMapAsync()
        {
            Map = MapManager.GetMap(MapIdentity);
            if (Map != null)
            {
                return Map.AddAsync(this);
            }
            return Task.CompletedTask;
        }

        public override async Task LeaveMapAsync()
        {
            await BroadcastRoomMsgAsync(new MsgMapItem
            {
                Identity = Identity,
                Itemtype = Mesh,
                MapX = X,
                MapY = Y,
                Mode = DropType.DropTrap
            }, false);

            if (Map != null)
            {
                Kernel.Services.Processor.Queue(Map.Partition, async () =>
                {
                    await Map.RemoveAsync(Identity);
                });
            }

            if (Identity >= MAGICTRAPID_FIRST && Identity <= MAGICTRAPID_LAST)
                IdentityManager.Traps.ReturnIdentity(Identity);
        }

        #endregion

        #region Trap Attack

        public async Task TrapAttackAsync(Role target)
        {
            if (!IsTrapSort)
                return;

            if (!fightTimer.ToNextTime(AttackSpeed))
                return;

            if (RemainingActiveTimes > 0)
                RemainingActiveTimes--;

            if (!target.IsAttackable(this))
                return;

            if (owner?.IsImmunity(target) == true)
                return;

            if (target is Character && !AttackMode.HasFlag(TargetType.User))
                return;

            if (target is Monster && !AttackMode.HasFlag(TargetType.Monster))
                return;

            Character user = owner as Character;
            if (user?.IsEvil() == true)
                await user.SetCrimeStatusAsync(30);

            if (!AttackMode.HasFlag(TargetType.Passive))
            {
                BattleSystem.CreateBattle(target.Identity);
                QueueAction(BattleSystem.ProcessAttackAsync);
            }

            if (IdAction > 0)
            {
                if (AttackMode.HasFlag(TargetType.User))
                    await GameAction.ExecuteActionAsync(IdAction, target as Character, this, null, "");
                else if (AttackMode.HasFlag(TargetType.Monster))
                    await GameAction.ExecuteActionAsync(IdAction, null, target, null, "");
                else
                    await GameAction.ExecuteActionAsync(IdAction, null, null, null, "");
            }

            if (ActiveTimes > 0 && RemainingActiveTimes <= 0)
                await LeaveMapAsync();
        }

        private async Task MagicAttackAsync()
        {
            if (magic == null || !IsAutoSort)
            {
                return;
            }

            switch (Sort)
            {
                case TrapTypeSort.BombTrap:
                    {
                        await ProcessTrapBombAsync(magic);
                        break;
                    }

                case TrapTypeSort.ThickLineTrap:
                    {
                        await ProcessThickLineTrapAsync(magic);
                        break;
                    }
            }
        }

        private async Task ProcessTrapBombAsync(Magic magic)
        {
            List<Role> roles = CollectTargetBomb(trap.Type.Size / 2);

            long battleExp = 0;
            var hitByMagic = MagicData.HitByMagic(magic);
            var user = owner as Character;
            foreach (Role target in roles)
            {
                if (magic.Ground != 0 && target.IsWing)
                {
                    continue;
                }

                var atkResult = await BattleSystem.CalcPowerAsync(hitByMagic, owner ?? this, target, magic.Power);
                int damage = atkResult.Damage;
                if (user?.IsLucky == true && await ChanceCalcAsync(1, 100))
                {
                    await user.SendEffectAsync("LuckyGuy", true);
                    damage *= 2;
                }

                damage += atkResult.ElementalDamage;

                await BroadcastRoomMsgAsync(new MsgInteract
                {
                    SenderIdentity = Identity,
                    TargetIdentity = target.Identity,
                    MagicType = magic.Type,
                    MagicLevel = magic.Level,
                    Data = damage,
                    PosX = X,
                    PosY = Y,
                    Action = MsgInteractType.Attack
                }, false);

                var lifeLost = (int)Math.Min(damage, target.Life);
                await target.BeAttackAsync(hitByMagic, this, damage, true);

                if (user != null && (target is Monster monster && monster.SpeciesType == 0 && !monster.IsGuard() && !monster.IsPkKiller() && !monster.IsRighteous() || (target as DynamicNpc)?.IsGoal() == true))
                {
                    battleExp += user.AdjustExperience(target, lifeLost, false);
                    if (!target.IsAlive)
                    {
                        var nBonusExp = (int)(target.MaxLife * 20 / 100d);
                        if (user.Team != null)
                        {
                            await user.Team.AwardMemberExpAsync(user.Identity, target, nBonusExp);
                        }
                        battleExp += user.AdjustExperience(target, nBonusExp, false);
                    }
                }

                if (user != null && target is DynamicNpc dynaNpc && dynaNpc.IsAwardScore())
                {
                    dynaNpc.AddSynWarScore(user.Syndicate, lifeLost);
                }

                if (user?.CurrentEvent != null)
                {
                    await user.CurrentEvent.OnHitAsync(user, target, magic);
                }

                if (user != null)
                {
                    await user.CheckCrimeAsync(target);
                }

                if (!target.IsAlive)
                {
                    await KillAsync(target, MagicData.GetDieMode());
                }
            }

            if (user?.CurrentEvent != null)
            {
                await user.CurrentEvent.OnAttackAsync(user);
            }
            
            if (user != null)
            {
                await user.MagicData.AwardExpAsync(0, battleExp, battleExp, magic);
            }
        }

        private async Task ProcessThickLineTrapAsync(Magic magic)
        {
            List<Point> testPosition = new();
            if (ActiveTimes - 1 == RemainingActiveTimes)
            {
                Calculations.DDALine(castPosition.X, castPosition.Y, X, Y, (int)(magic.Range * (magic.ActiveTimes + 1)), ref testPosition);
                destinationPoint = testPosition.LastOrDefault();
            }

            int range = (int)(magic.Range);
            var targets = CollectTargetBomb(range);
            testPosition.Clear();
            Calculations.DDALine(castPosition.X, castPosition.Y, destinationPoint.X, destinationPoint.Y, range, ref testPosition);
            Point destination = testPosition.LastOrDefault();
            var bresenham = Bresenham.CalculateThick(castPosition.X, castPosition.Y, destination.X, destination.Y, magic.Width);

#if DEBUG
            if (owner is Character gm && gm.IsGm())
            {
                await owner.SendAsync($"DDA from[{castPosition.X},{castPosition.Y}] To[{destination.X},{destination.Y}] Length: {testPosition.Count}", TalkChannel.Talk, Color.White);
            }
#endif

            int power1 = (int)magic.StatusData2;
            int power2 = (int)magic.DurTime;
            int power3 = (int)magic.AtkInterval;

            MsgMagicEffect msg = new MsgMagicEffect
            {
                AttackerIdentity = owner.Identity,
                Command = owner.Identity,
                MagicIdentity = magic.Type,
                MagicLevel = magic.Level
            };

            Character user = owner as Character;
            var hitByMagic = MagicData.HitByMagic(magic);
            long battleExp = 0;

            Dictionary<uint, int> setDamage = new();
            List<Role> setTarget = new();
            foreach (var target in targets)
            {
                if (!bresenham.Any(point => point.X == target.X && point.Y == target.Y))
                {
                    continue;
                }

                int damage = 0;
                InteractionEffect effect = InteractionEffect.None;
                if (!await target.CheckScapegoatAsync(owner))
                {
                    if (msg.Count >= MagicData.MAX_TARGET_NUM)
                    {
                        await owner.BroadcastRoomMsgAsync(msg, true);
                        msg.ClearTargets();
                    }

                    if (user != null)
                    {
                        await user.CheckCrimeAsync(target);
                    }

                    int powerSector = (int)Math.Ceiling(owner.GetDistance(target) / 3d);
                    int power;
                    if (RemainingActiveTimes == 3)
                    {
                        power = power1;
                    }
                    else if (RemainingActiveTimes == 2)
                    {
                        power = power2;
                    }
                    else
                    {
                        power = power3;
                    }

                    var result = await BattleSystem.CalcPowerAsync(hitByMagic, owner, target, power);
                    damage = result.Damage + result.ElementalDamage;
                    effect = result.Effect;
                }

                msg.Append(target.Identity, damage, damage != 0, effect, damage);

                setDamage.Add(target.Identity, damage);
                setTarget.Add(target);
            }

            if (msg.Count > 0)
            {
                await owner.BroadcastRoomMsgAsync(msg, true);
            }

            await owner.BroadcastRoomMsgAsync(new MsgAction
            {
                Action = (MsgAction.ActionType)434,
                Identity = Identity,
                CommandX = (ushort)destination.X,
                CommandY = (ushort)destination.Y,
                X = X,
                Y = Y
            }, true);

            foreach (var target in setTarget)
            {
                int damage = setDamage[target.Identity];
                var lifeLost = (int)Math.Min(damage, target.Life);
                await target.BeAttackAsync(hitByMagic, this, damage, true);
                if (user != null && (target is Monster monster && monster.SpeciesType == 0 && !monster.IsGuard() && !monster.IsPkKiller() && !monster.IsRighteous() || (target as DynamicNpc)?.IsGoal() == true))
                {
                    battleExp += user.AdjustExperience(target, lifeLost, false);
                    if (!target.IsAlive)
                    {
                        var nBonusExp = (int)(target.MaxLife * 20 / 100d);
                        if (user.Team != null)
                        {
                            await user.Team.AwardMemberExpAsync(user.Identity, target, nBonusExp);
                        }
                        battleExp += user.AdjustExperience(target, nBonusExp, false);
                    }
                }

                if (user != null && target is DynamicNpc dynaNpc && dynaNpc.IsAwardScore())
                {
                    dynaNpc.AddSynWarScore(user.Syndicate, lifeLost);
                }

                if (user?.CurrentEvent != null)
                {
                    await user.CurrentEvent.OnHitAsync(user, target, magic);
                }

                if (!target.IsAlive)
                {
                    await KillAsync(target, MagicData.GetDieMode());
                }
            }

            if (user?.CurrentEvent != null)
            {
                await user.CurrentEvent.OnAttackAsync(user);
            }

            if (user != null)
            {
                await user.MagicData.AwardExpAsync(0, battleExp, battleExp, magic);
            }

            castPosition = new(destination.X, destination.Y);
        }

        private List<Role> CollectTargetBomb(int range)
        {
            List<Role> targets = new();
            List<Role> setRoles = Map.Query9BlocksByPos(X, Y);
            foreach (Role target in setRoles)
            {
                if (target.Identity == Identity || target.Identity == owner?.Identity)
                {
                    continue;
                }

                if (target.GetDistance(X, Y) > range)
                {
                    continue;
                }

                bool isCharacter = target is Character;
                bool isMonster = target is Monster;
                if (!AttackMode.HasFlag(TargetType.User) && isCharacter)
                {
                    continue;
                }

                //if (!AttackMode.HasFlag(TargetType.Monster) && isMonster)
                //{
                //    continue;
                //}

                if (!isCharacter && !isMonster)
                {
                    continue;
                }

                Role role = owner ?? this;
                if (role.IsImmunity(target) || !target.IsAttackable(owner ?? this))
                {
                    continue;
                }

                targets.Add(target);
            }
            return targets;
        }

        public override async Task KillAsync(Role target, uint dieWay)
        {
            if (target == null)
                return;

            if (owner != null)
            {
                await owner.KillAsync(target, dieWay);
                return;
            }

            await BroadcastRoomMsgAsync(new MsgInteract
            {
                Action = MsgInteractType.Kill,
                SenderIdentity = Identity,
                TargetIdentity = target.Identity,
                PosX = target.X,
                PosY = target.Y,
                Data = (int)dieWay
            }, true);

            await target.BeKillAsync(this);
        }

        #endregion

        #region Timer

        public override async Task OnTimerAsync()
        {
            if (!IsAlive && !IsAutoSort)
                return;

            if (lifePeriodTimer.IsActive())
            {
                if (lifePeriodTimer.ToNextTime())
                {
                    await LeaveMapAsync();
                    lifePeriodTimer.Clear();
                }
            }

            if (IsAutoSort)
            {
                if (fightTimer.ToNextTick(AttackSpeed))
                {
                    if (ActiveTimes > 0)
                        RemainingActiveTimes--;

                    await MagicAttackAsync();

                    if ((ActiveTimes > 0 || IsAutoSort) && RemainingActiveTimes <= 0)
                    {
                        await LeaveMapAsync();
                        IdentityManager.Traps.ReturnIdentity(Identity);
                    }
                }
            }
        }

        #endregion

        #region Socket

        public override Task SendSpawnToAsync(Character player)
        {
            return player.SendAsync(new MsgMapItem
            {
                Identity = Identity,
                Itemtype = Mesh,
                MapX = X,
                MapY = Y,
                Mode = DropType.SynchroTrap
            });
        }

        #endregion

        [Flags]
        public enum TargetType
        {
            None,
            User = 1,
            Monster = 2,
            Passive = 4
        }

        public enum TrapTypeSort
        {
            SystemTrap = 1,
            TrapSort = 10,
            BombTrap = 11,
            ThickLineTrap = 12
        }
    }
}
