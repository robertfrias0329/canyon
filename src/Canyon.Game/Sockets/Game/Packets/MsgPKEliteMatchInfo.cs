﻿using Canyon.Game.States.User;
using Canyon.Network.Packets;
using Newtonsoft.Json;
using static Canyon.Game.States.Events.Interfaces.ITournamentEventParticipant<Canyon.Game.States.User.Character>;
using static Canyon.Game.States.Events.Tournament.BaseTournamentMatch<Canyon.Game.States.Events.Elite.ElitePkParticipant, Canyon.Game.States.User.Character>;

namespace Canyon.Game.Sockets.Game.Packets
{
    public sealed class MsgPkEliteMatchInfo : MsgBase<Client>
    {
        private static readonly ILogger logger = LogFactory.CreateLogger<MsgPkEliteMatchInfo>();

        private static readonly MatchInfo[] DebugInfo =
        {
            new MatchInfo
            {
                MatchIdentity = 400000,
                Index = 0,
                Status = MatchStatus.AcceptingWagers,
                ContestantInfos = new List<MatchContestantInfo>
                {
                    new MatchContestantInfo
                    {
                        Identity = 1_000_100,
                        Name = "Test100[PM]",
                        Mesh = 21003,
                        Flag = ContestantFlag.None,
                        ServerId = 0,
                        Winner = false
                    },
                    new MatchContestantInfo
                    {
                        Identity = 1_000_101,
                        Name = "Test101[PM]",
                        Mesh = 11003,
                        Flag = ContestantFlag.None,
                        ServerId = 0,
                        Winner = false
                    },
                }
            },
            new MatchInfo
            {
                MatchIdentity = 400001,
                Index = 1,
                Status = MatchStatus.AcceptingWagers,
                ContestantInfos = new List<MatchContestantInfo>
                {
                    new MatchContestantInfo
                    {
                        Identity = 1_000_102,
                        Name = "Test102[PM]",
                        Mesh = 21003,
                        Flag = ContestantFlag.None,
                        ServerId = 0,
                        Winner = false
                    },
                    new MatchContestantInfo
                    {
                        Identity = 1_000_103,
                        Name = "Test103[PM]",
                        Mesh = 11003,
                        Flag = ContestantFlag.None,
                        ServerId = 0,
                        Winner = false
                    },
                }
            },
            new MatchInfo
            {
                MatchIdentity = 400002,
                Index = 2,
                Status = MatchStatus.AcceptingWagers,
                ContestantInfos = new List<MatchContestantInfo>
                {
                    new MatchContestantInfo
                    {
                        Identity = 1_000_104,
                        Name = "Test104[PM]",
                        Mesh = 21003,
                        Flag = ContestantFlag.None,
                        ServerId = 0,
                        Winner = false
                    },
                    new MatchContestantInfo
                    {
                        Identity = 1_000_105,
                        Name = "Test105[PM]",
                        Mesh = 11003,
                        Flag = ContestantFlag.None,
                        ServerId = 0,
                        Winner = false
                    },
                }
            },
            new MatchInfo
            {
                MatchIdentity = 400003,
                Index = 3,
                Status = MatchStatus.AcceptingWagers,
                ContestantInfos = new List<MatchContestantInfo>
                {
                    new MatchContestantInfo
                    {
                        Identity = 1_000_106,
                        Name = "Test106[PM]",
                        Mesh = 21003,
                        Flag = ContestantFlag.None,
                        ServerId = 0,
                        Winner = false
                    },
                    new MatchContestantInfo
                    {
                        Identity = 1_000_107,
                        Name = "Test107[PM]",
                        Mesh = 11003,
                        Flag = ContestantFlag.None,
                        ServerId = 0,
                        Winner = false
                    },
                }
            },
            new MatchInfo
            {
                MatchIdentity = 400004,
                Index = 4,
                Status = MatchStatus.AcceptingWagers,
                ContestantInfos = new List<MatchContestantInfo>
                {
                    new MatchContestantInfo
                    {
                        Identity = 1_000_108,
                        Name = "Test108[PM]",
                        Mesh = 21003,
                        Flag = ContestantFlag.None,
                        ServerId = 0,
                        Winner = false
                    },
                    new MatchContestantInfo
                    {
                        Identity = 1_000_109,
                        Name = "Test109[PM]",
                        Mesh = 11003,
                        Flag = ContestantFlag.None,
                        ServerId = 0,
                        Winner = false
                    },
                }
            },
            new MatchInfo
            {
                MatchIdentity = 400005,
                Index = 5,
                Status = MatchStatus.AcceptingWagers,
                ContestantInfos = new List<MatchContestantInfo>
                {
                    new MatchContestantInfo
                    {
                        Identity = 1_000_110,
                        Name = "Test110[PM]",
                        Mesh = 21003,
                        Flag = ContestantFlag.None,
                        ServerId = 0,
                        Winner = false
                    },
                    new MatchContestantInfo
                    {
                        Identity = 1_000_111,
                        Name = "Test111[PM]",
                        Mesh = 11003,
                        Flag = ContestantFlag.None,
                        ServerId = 0,
                        Winner = false
                    },
                }
            },
            new MatchInfo
            {
                MatchIdentity = 400006,
                Index = 6,
                Status = MatchStatus.AcceptingWagers,
                ContestantInfos = new List<MatchContestantInfo>
                {
                    new MatchContestantInfo
                    {
                        Identity = 1_000_112,
                        Name = "Test112[PM]",
                        Mesh = 21003,
                        Flag = ContestantFlag.None,
                        ServerId = 0,
                        Winner = false
                    },
                    new MatchContestantInfo
                    {
                        Identity = 1_000_113,
                        Name = "Test113[PM]",
                        Mesh = 11003,
                        Flag = ContestantFlag.None,
                        ServerId = 0,
                        Winner = false
                    },
                }
            },
            new MatchInfo
            {
                MatchIdentity = 400007,
                Index = 7,
                Status = MatchStatus.AcceptingWagers,
                ContestantInfos = new List<MatchContestantInfo>
                {
                    new MatchContestantInfo
                    {
                        Identity = 1_000_114,
                        Name = "Test114[PM]",
                        Mesh = 21003,
                        Flag = ContestantFlag.None,
                        ServerId = 0,
                        Winner = false
                    },
                    new MatchContestantInfo
                    {
                        Identity = 1_000_115,
                        Name = "Test115[PM]",
                        Mesh = 11003,
                        Flag = ContestantFlag.None,
                        ServerId = 0,
                        Winner = false
                    },
                }
            },
            new MatchInfo
            {
                MatchIdentity = 400008,
                Index = 8,
                Status = MatchStatus.AcceptingWagers,
                ContestantInfos = new List<MatchContestantInfo>
                {
                    new MatchContestantInfo
                    {
                        Identity = 1_000_116,
                        Name = "Test116[PM]",
                        Mesh = 21003,
                        Flag = ContestantFlag.None,
                        ServerId = 0,
                        Winner = false
                    },
                    new MatchContestantInfo
                    {
                        Identity = 1_000_117,
                        Name = "Test117[PM]",
                        Mesh = 11003,
                        Flag = ContestantFlag.None,
                        ServerId = 0,
                        Winner = false
                    },
                }
            },
            new MatchInfo
            {
                MatchIdentity = 400009,
                Index = 9,
                Status = MatchStatus.AcceptingWagers,
                ContestantInfos = new List<MatchContestantInfo>
                {
                    new MatchContestantInfo
                    {
                        Identity = 1_000_118,
                        Name = "Test118[PM]",
                        Mesh = 21003,
                        Flag = ContestantFlag.None,
                        ServerId = 0,
                        Winner = false
                    },
                    new MatchContestantInfo
                    {
                        Identity = 1_000_119,
                        Name = "Test119[PM]",
                        Mesh = 11003,
                        Flag = ContestantFlag.None,
                        ServerId = 0,
                        Winner = false
                    },
                }
            },
            new MatchInfo
            {
                MatchIdentity = 400010,
                Index = 10,
                Status = MatchStatus.AcceptingWagers,
                ContestantInfos = new List<MatchContestantInfo>
                {
                    new MatchContestantInfo
                    {
                        Identity = 1_000_120,
                        Name = "Test120[PM]",
                        Mesh = 21003,
                        Flag = ContestantFlag.None,
                        ServerId = 0,
                        Winner = false
                    },
                    new MatchContestantInfo
                    {
                        Identity = 1_000_121,
                        Name = "Test121[PM]",
                        Mesh = 11003,
                        Flag = ContestantFlag.None,
                        ServerId = 0,
                        Winner = false
                    },
                }
            },
            new MatchInfo
            {
                MatchIdentity = 400011,
                Index = 11,
                Status = MatchStatus.AcceptingWagers,
                ContestantInfos = new List<MatchContestantInfo>
                {
                    new MatchContestantInfo
                    {
                        Identity = 1_000_122,
                        Name = "Test122[PM]",
                        Mesh = 21003,
                        Flag = ContestantFlag.None,
                        ServerId = 0,
                        Winner = false
                    },
                    new MatchContestantInfo
                    {
                        Identity = 1_000_123,
                        Name = "Test123[PM]",
                        Mesh = 11003,
                        Flag = ContestantFlag.None,
                        ServerId = 0,
                        Winner = false
                    },
                }
            },
            new MatchInfo
            {
                MatchIdentity = 400012,
                Index = 12,
                Status = MatchStatus.OK,
                ContestantInfos = new List<MatchContestantInfo>
                {
                    new MatchContestantInfo
                    {
                        Identity = 1_000_124,
                        Name = "Test124[PM]",
                        Mesh = 21003,
                        Flag = ContestantFlag.Qualified,
                        ServerId = 0,
                        Winner = true
                    }
                }
            }
        };

        public ElitePkMatchType Mode { get; set; }
        public ushort Page { get; set; }
        public ushort MsgIndex { get; set; }
        public int TotalMatches { get; set; }
        public ushort Group { get; set; }
        public ElitePkGuiType Gui { get; set; }
        public ushort TimeLeft { get; set; }
        public int MatchCount { get; set; }
        public List<MatchInfo> Matches { get; set; } = new();

        public override void Decode(byte[] bytes)
        {
            using var reader = new PacketReader(bytes);
            Length = reader.ReadUInt16();
            Type = (PacketType)reader.ReadUInt16();
            Mode = (ElitePkMatchType)reader.ReadUInt16();
            Page = reader.ReadUInt16();
            MsgIndex = reader.ReadUInt16();
            TotalMatches = reader.ReadInt32();
            Group = reader.ReadUInt16();
            Gui = (ElitePkGuiType)reader.ReadUInt16();
            TimeLeft = reader.ReadUInt16();
            MatchCount = reader.ReadInt32();
        }

        public override byte[] Encode()
        {
            using var writer = new PacketWriter();
            writer.Write((ushort)PacketType.MsgPkEliteMatchInfo);
            writer.Write((ushort)Mode); // 4
            writer.Write(Page); // 6
            writer.Write(MsgIndex); // 8
            writer.Write(TotalMatches); // 10
            writer.Write(Group); // 14
            writer.Write((ushort)Gui); // 16
            writer.Write(TimeLeft); // 18
            writer.Write(MatchCount = Matches.Count); // 20

            foreach (var match in Matches)
            {
                writer.Write(match.MatchIdentity); // 0
                writer.Write((ushort)match.ContestantInfos.Count); // 4
                writer.Write((ushort)match.Index); // 6
                writer.Write((ushort)match.Status); // 8
                foreach (var contestant in match.ContestantInfos)
                {
                    writer.Write(contestant.Identity); // 0
                    writer.Write(contestant.Mesh); // 4
                    writer.Write(contestant.Name, 16); // 8
                    writer.Write((int)contestant.Flag); // 24
                    writer.Write((ushort)(contestant.Winner ? 1 : 0)); // 28
                }

                if (match.ExtraInfo.Id == 0)
                {
                    int fillSize = Math.Max(0, 3 - match.ContestantInfos.Count) * 30;
                    writer.Write(new byte[fillSize]);
                }
                else
                {
                    writer.Write(match.ExtraInfo.Unknown0);
                    writer.Write(match.ExtraInfo.Unknown4);
                    writer.Write(match.ExtraInfo.MatchIndex);
                    writer.Write((int)match.ExtraInfo.Status);
                    writer.Write(match.ExtraInfo.Group);
                    writer.Write(match.ExtraInfo.Id);
                    writer.Write(match.ExtraInfo.Unknown22);
                    writer.Write(match.ExtraInfo.Name, 4);
                }
            }
            return writer.ToArray();
        }

        public override async Task ProcessAsync(Client client)
        {
            Character user = client.Character;
            const int ipp = 5;

            ElitePkGuiType gui = ElitePkGuiType.Knockout16;
            switch (Mode)
            {
                case ElitePkMatchType.RequestInformation:
                    {
                        // if ElitePK is running, send Gui and Timeleft [timeleft=1 running or seconds for waiting].
                        //TimeLeft = 60;
                        //Gui = gui;
                        await user.SendAsync(this);
                        break;
                    }

                //case ElitePkMatchType.MainPage:
                //    {
                //        MsgPkEliteMatchInfo msg = new();
                //        msg.Decode(Encode());
                //        msg.TimeLeft = 1;
                //        msg.TotalMatches = DebugInfo.Length;
                //        await user.SendAsync(this);

                //        Mode = ElitePkMatchType.GuiUpdate;
                //        await user.SendAsync(this);

                //        Mode = ElitePkMatchType.UpdateList;
                //        if (gui == ElitePkGuiType.Knockout16)
                //        {
                //            TimeLeft = 60;
                //            TotalMatches = DebugInfo.Length;
                //            foreach (var match in DebugInfo
                //                .Skip(ipp * Page)
                //                .Take(ipp))
                //            {
                //                Matches.Add(match);
                //            }
                //        }
                //        else if (gui == ElitePkGuiType.Top8Qualifier)
                //        {
                //            var top8 = DebugInfo.Take(8).ToArray();

                //            TimeLeft = 60;
                //            TotalMatches = top8.Length;
                //            foreach (var match in top8)
                //            {
                //                Matches.Add(match);
                //            }
                //        }

                //        await user.SendAsync(this);
                //        break;
                //    }

                default:
                    {
                        logger.LogWarning("Mode [{Action}] is not being handled.\n{Json}", Mode, JsonConvert.SerializeObject(this));
                        break;
                    }
            }
        }

        public enum ElitePkMatchType : ushort
        {
            MainPage = 0,
            StaticUpdate = 1,
            GuiUpdate = 2,
            UpdateList = 3,
            RequestInformation = 4,
            StopWagers = 5,
            EventState = 6
        }

        public enum ElitePkGuiType : ushort
        {
            Top8Ranking = 0,
            Knockout = 2,
            Knockout16 = 3,
            Top8Qualifier = 4,
            Top4Qualifier = 5,
            Top2Qualifier = 6,
            Top3Qualifier = 7,
            Top1Qualifier = 8,
            ReconstructTop = 9
        }

        public struct MatchInfo
        {
            public uint MatchIdentity { get; set; }
            public int Index { get; set; } // ??
            public MatchStatus Status { get; set; }
            public List<MatchContestantInfo> ContestantInfos { get; set; }
            public MatchContestantLostInfo ExtraInfo { get; set; }
        }

        public struct MatchContestantInfo
        {
            public uint Identity { get; set; }
            public uint Mesh { get; set; }
            public string Name { get; set; }
            public int ServerId { get; set; }
            public ContestantFlag Flag { get; set; }
            public bool Winner { get; set; } // ushort
        }

        public struct MatchContestantLostInfo
        {
            public int Unknown0 { get; set; } // 0
            public ushort Unknown4 { get; set; } // 4
            public int MatchIndex { get; set; } // 6
            public MatchStatus Status { get; set; } // 10
            public int Group { get; set; } // 14
            public uint Id { get; set; } // 18
            public int Unknown22 { get; set; } // 22
            public string Name { get; set; } // 26
        }
    }
}
