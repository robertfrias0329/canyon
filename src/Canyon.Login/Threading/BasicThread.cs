﻿using Canyon.Login.Managers;
using Quartz;

namespace Canyon.Login.Threading
{
    [DisallowConcurrentExecution]
    public sealed class BasicThread : IJob
    {
        private const string CONSOLE_TITLE = "Conquer Online Login Server - Servers[{0}], Players[{1}] - {2}";

        public BasicThread()
        {
        }

        public Task Execute(IJobExecutionContext context)
        {
            Console.Title = string.Format(CONSOLE_TITLE, RealmManager.Count, PlayerManager.Count, DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff"));



            return Task.CompletedTask;
        }

    }
}
