﻿using Canyon.Database.Entities;
using Canyon.Login.Managers;
using Canyon.Login.Repositories;
using Canyon.Login.States.Requests;
using Canyon.Network.Packets.Login;
using Canyon.Network.RPC;
using Canyon.Shared;
using Microsoft.Extensions.Logging;

namespace Canyon.Login.Sockets.Game
{
    public class GameRemoteRpc : IRpcServerTarget
    {
        private static readonly ILogger logger = LogFactory.CreateLogger<GameRemoteRpc>();

        private string agentName;

        public Task ConnectedAsync(string agentName)
        {
            this.agentName = agentName;
            logger.LogInformation("{} has connected", agentName);
            return Task.CompletedTask;
        }

        public Task ChangePlayerStatusAsync(MsgAccServerPlayerStatus playerStatus)
        {
            return Task.CompletedTask;
        }

        public Task UpdateRealmInformationAsync(MsgAccServerGameInformation gameInformation)
        {
            DbRealm.RealmStatus status = DbRealm.RealmStatus.Online;
            if (gameInformation.PlayerCount > 300)
            {
                status = DbRealm.RealmStatus.Busy;
            }

            var realm = RealmManager.GetRealm(agentName);
            if (realm == null)
            {
                logger.LogError("Invalid realm '{}' for information update", agentName);
                return Task.CompletedTask;
            }

            return RealmsRepository.SyncRealmAsync(new RealmSyncRequest
            {
                RealmId = realm.RealmID,
                CurrentStatus = (int)status,
                CurrentOnlinePlayers = gameInformation.PlayerCount,
                MaxOnlinePlayers = gameInformation.PlayerCountRecord
            });
        }
    }
}
