﻿using Canyon.Login.Managers;
using Canyon.Login.States;
using Canyon.Login.States.Requests;
using Canyon.Login.States.Responses;
using Canyon.Network.Packets.Login;
using Canyon.Network.RPC.Models.Login.Auth;
using Canyon.Shared;
using Microsoft.Extensions.Logging;

namespace Canyon.Login.Sockets.Login.Packets
{
    public sealed class MsgAccount : MsgAccount<Client>
    {
        private static readonly ILogger logger = LogFactory.CreateLogger<MsgAccount>();

        public async override Task ProcessAsync(Client client)
        {
            try
            {
                //logger.LogInformation("Attempting to authenticate user [{}]", Username);
                GameAccountLoginResponse response = await Kernel.RestClient.PostAsync<GameAccountLoginResponse>($"{Kernel.ServerConfiguration.Authentication.Url}/api/auth/login", new GameAccountLoginRequest
                {
                    UserName = Username,
                    Password = DecryptPassword(Password, (uint)client.Seed),
                    ServerName = Realm
                });
                if (!response.Success)
                {
                    logger.LogInformation("User [{}] attempt failed: invalid password", Username);
                    await client.SendAsync(new MsgConnectEx(MsgConnectEx<Client>.RejectionCode.InvalidPassword), () => { client.Disconnect(); return Task.CompletedTask; });
                    return;
                }

                if (response.IsPermanentlyBanned)
                {
                    logger.LogInformation("User [{}] attempt failed: permanent ban", Username);
                    await client.SendAsync(new MsgConnectEx(MsgConnectEx<Client>.RejectionCode.AccountBanned), () => { client.Disconnect(); return Task.CompletedTask; });
                    return;
                }

                if (response.IsBanned)
                {
                    logger.LogInformation("User [{}] attempt failed: banned", Username);
                    await client.SendAsync(new MsgConnectEx(MsgConnectEx<Client>.RejectionCode.AccountBanned), () => { client.Disconnect(); return Task.CompletedTask; });
                    return;
                }

                if (response.IsLocked)
                {
                    logger.LogInformation("User [{}] attempt failed: locked", Username);
                    await client.SendAsync(new MsgConnectEx(MsgConnectEx<Client>.RejectionCode.AccountLocked), () => { client.Disconnect(); return Task.CompletedTask; });
                    return;
                }

#if DEBUG
                // only testing
                if (response.AccountAuthority == 1)
                {
                    logger.LogInformation("User [{}] attempt failed: non cooperator account", Username);
                    await client.SendAsync(new MsgConnectEx(MsgConnectEx<Client>.RejectionCode.NonCooperatorAccount), () =>
                    {
                        client.Disconnect();
                        return Task.CompletedTask;
                    });
                    return;
                }
#endif

                client.AccountID = (uint)response.AccountId;

                var realm = RealmManager.GetRealm(Realm);
                if (realm == null)
                {
                    logger.LogInformation("User [{}] realm '{}' not configured", Username, Realm);
                    await client.SendAsync(new MsgConnectEx(MsgConnectEx<Client>.RejectionCode.ServersNotConfigured), () =>
                    {
                        client.Disconnect();
                        return Task.CompletedTask;
                    });
                    return;
                }

                if (!realm.Data.Active)
                {
                    logger.LogInformation("User [{}] realm '{}' not active", Username, realm.Data.RealmName);
                    await client.SendAsync(new MsgConnectEx(MsgConnectEx<Client>.RejectionCode.ServerLocked), () => { client.Disconnect(); return Task.CompletedTask; });
                    return;
                }

                if (!realm.Rpc.Online)
                {
                    logger.LogInformation("User [{}] realm '{}' not online", Username, realm.Data.RealmName);
                    await client.SendAsync(new MsgConnectEx(MsgConnectEx<Client>.RejectionCode.ServerDown), () =>
                    {
                        client.Disconnect();
                        return Task.CompletedTask;
                    });
                    return;
                }

                client.Realm = realm;

                ClientManager.AddClient(client);
                //logger.LogInformation($"Client [{client.Guid}] accountID {client.AccountID} is awaiting for authorization");

                var gameAuthResult = await realm.Rpc.CallAsync<TransferAuthResponse>("TransferAuth", new TransferAuthRequest
                {
                    AccountID = client.AccountID,
                    AuthorityID = (ushort)response.AccountAuthority,
                    IPAddress = client.IpAddress,
                });

                if (gameAuthResult == null)
                {
                    logger.LogInformation("User [{}] '{}' auth failed. Null result", Username, realm.Data.RealmName);
                    await client.SendAsync(new MsgConnectEx(MsgConnectEx<Client>.RejectionCode.ServerBusy), () =>
                    {
                        client.Disconnect();
                        return Task.CompletedTask;
                    });
                    return;
                }

                if (gameAuthResult.Code != TransferAuthResponse.ResultCode.Success)
                {
                    logger.LogInformation("User [{}] '{}' auth failed. Result [{}]", Username, realm.Data.RealmName, gameAuthResult.Message);
                    await client.SendAsync(new MsgConnectEx(MsgConnectEx<Client>.RejectionCode.ServerBusy), () =>
                    {
                        client.Disconnect();
                        return Task.CompletedTask;
                    });
                }

                await client.SendAsync(new MsgConnectEx(client.Realm.Data.GameIPAddress, client.Realm.Data.GamePort, gameAuthResult.Token), () =>
                {
                    client.Disconnect();
                    return Task.CompletedTask;
                });
                logger.LogInformation($"[{Username}] has authenticated successfully on [{client.Realm.Data.RealmName}].");
                return;
            }
            catch (Exception ex)
            {
                await client.SendAsync(new MsgConnectEx(MsgConnectEx<Client>.RejectionCode.ServerTimedOut), () =>
                {
                    client.Disconnect();
                    return Task.CompletedTask;
                });
                logger.LogCritical(ex, "{}", ex.Message);
            }
        }
    }
}
