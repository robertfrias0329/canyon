﻿using Canyon.Login.Managers;
using Canyon.Login.Sockets.Game;
using Canyon.Login.Sockets.Login;
using Canyon.Login.Threading;
using Canyon.Network.RPC;
using Canyon.Network.Services;
using Canyon.Shared.Loggers;
using Canyon.Shared.Rest;
using Canyon.Shared.Threads;

namespace Canyon.Login
{
    public static class Kernel
    {
        private static SchedulerFactory SchedulerFactory { get; set; }

        public static LogProcessor LogProcessor { get; set; }
        public static ServerConfiguration ServerConfiguration { get; set; }
        public static SocketConnection Sockets = new();
        public static RestClient RestClient = new();

        public static async Task<bool> StartUpAsync(ServerConfiguration serverConfiguration)
        {
            await RestClient.AuthorizeAsync(ServerConfiguration.Authentication.Identity, ServerConfiguration.Authentication.ClientId, ServerConfiguration.Authentication.ClientSecret, "server_auth realm_validate");

            await RealmManager.InitializeAsync().ConfigureAwait(true);

            Sockets.GameServer = new RpcServerListener(new GameRemoteRpc());
            _ = Sockets.GameServer.StartAsync(serverConfiguration.RealmNetwork.Port, serverConfiguration.RealmNetwork.IPAddress);

            Sockets.LoginServer = new LoginServer(serverConfiguration);
            _ = Sockets.LoginServer.StartAsync(serverConfiguration.Network.Port);

            SchedulerFactory = new SchedulerFactory();
            await SchedulerFactory.StartAsync();
            await SchedulerFactory.ScheduleAsync<BasicThread>("* * * * * ?");
            return true;
        }

        public static async Task ShutdownAsync()
        {
            await SchedulerFactory.StopAsync();
        }

        public static Task<int> NextAsync(int minValue, int maxValue) => RandomnessService.NextAsync(minValue, maxValue);

        public class SocketConnection
        {
            public LoginServer LoginServer { get; set; }
            public RpcServerListener GameServer { get; set; }
        }
    }
}
