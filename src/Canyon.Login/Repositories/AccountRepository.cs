﻿using Canyon.Login.States.Responses;
using Canyon.Shared;
using Microsoft.Extensions.Logging;

namespace Canyon.Login.Repositories
{
    public class AccountRepository
    {
        private static readonly ILogger logger = LogFactory.CreateLogger<AccountRepository>();

        private AccountRepository() { }

        public static Task<GameAccountResponse> FindAsync(uint accountId)
        {
            // Load realm connection information
            return Kernel.RestClient.GetAsync<GameAccountResponse>($"{Kernel.ServerConfiguration.Account.Url}/api/conquer/account/{accountId}");
        }
    }
}
