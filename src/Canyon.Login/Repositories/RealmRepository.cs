﻿using Canyon.Login.States.Requests;
using Canyon.Login.States.Responses;

namespace Canyon.Login.Repositories
{
    /// <summary>
    ///     Repository for defining data access layer (DAL) logic for the realm table. Realm
    ///     connection details are loaded into server memory at server startup, and may be
    ///     modified once loaded.
    /// </summary>
    public class RealmsRepository
    {
        /// <summary>
        ///     Loads realm connection details and security information to the server's pool
        ///     of known realm routes. Should be invoked at server startup before the server
        ///     listener has been started.
        /// </summary>
        public static Task<RealmDataResponse> FindAsync(string realmName)
        {
            // Load realm connection information
            return Kernel.RestClient.GetAsync<RealmDataResponse>($"{Kernel.ServerConfiguration.Realm.Url}/api/realms/find-by-name/{realmName}");
        }

        public static Task<RealmDataResponse> FindByIdAsync(string uuid)
        {
            // Load realm connection information
            return Kernel.RestClient.GetAsync<RealmDataResponse>($"{Kernel.ServerConfiguration.Realm.Url}/api/realms/find-by-id/{uuid}");
        }

        public static Task<List<RealmDataResponse>> QueryRealmsAsync()
        {
            return Kernel.RestClient.GetAsync<List<RealmDataResponse>>($"{Kernel.ServerConfiguration.Realm.Url}/api/realms");
        }

        public static Task SyncRealmAsync(RealmSyncRequest body)
        {
            return Kernel.RestClient.PutAsync<object>($"{Kernel.ServerConfiguration.Realm.Url}/api/realms/status/{body.RealmId}", body);
        }
    }
}
