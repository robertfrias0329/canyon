﻿using Canyon.Login.States.Responses;
using Canyon.Network.RPC;

namespace Canyon.Login.States
{
    public sealed class Realm
    {
        public RpcClient Rpc { get; init; }

        public Guid RealmID => Data?.RealmID ?? Guid.Empty;

        public RealmDataResponse Data { get; init; }
    }
}
